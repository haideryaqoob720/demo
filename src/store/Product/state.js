export default {
  allProducts: [],
  productDetails: 0,
  productImages: [],
  // model
  tradeModel: false,
  modelType: "bids",
  modelData: [],
  // asks bids
  bids: [],
  asks: [],
  // placing ask or bid
  userId: null,
  userName: "",
  productId: null,
  productName: "",
  // bid ask amount
  bid: 0,
  ask: 0,
};
