import io from "socket.io-client";
import router from "./../../router/index";

const socket = io("http://54.189.70.25");

export default {
  getAllProducts: (state, rootState) => {
    rootState.loading = true;
    axios.get("/allProducts/1").then(({ data }) => {
      state.allProducts = data.rows;
      console.log("all products", state.allProducts);
      rootState.loading = false;
    });
  },
  getProduct: (state) => {},
  getBids: (state, rootState) => {
    rootState.loading = true;
    socket.emit("getbid", { product: state.productId });
    socket.on("initial bids", (data) => {
      rootState.loading = false;
      state.bids = data;
    });
  },
  getAsks: (state, rootState) => {
    rootState.loading = true;
    socket.emit("getask", { product: state.productId });
    socket.on("initial asks", (data) => {
      rootState.loading = false;
      state.asks = data;
    });
  },
  // PLACE BID
  placeBid: (state, { bidAmount, rootState }) => {
    rootState.loading = true;
    const toast = rootState.toast;
    socket.emit("new bid", {
      userid: localStorage.getItem("userId"),
      username: localStorage.getItem("userName"),
      productid: state.productId,
      productname: state.productName,
      bid: bidAmount,
      token: localStorage.getItem("token"),
    });
    socket.on("Bid Placed", () => {
      rootState.loading = false;
      [toast.watcher, toast.type, toast.message] = [
        Math.random(),
        "success",
        "Bid is successfully Placed",
      ];
      router.go(-1);
    });
    socket.on("twice bid", () => {
      rootState.loading = false;
      [toast.watcher, toast.type, toast.message] = [
        Math.random(),
        "success",
        "You cannot place same bid twice, update it",
      ];
    });
  },
  // PLACE ASK
  placeAsk: (state, { askAmount, rootState }) => {
    rootState.loading = true;
    const toast = rootState.toast;
    socket.emit("new ask", {
      userids: localStorage.getItem("userId"),
      usernames: localStorage.getItem("userName"),
      ask: askAmount,
      productids: state.productId,
      productnames: state.productName,
    });
    socket.on("askplaced", () => {
      [toast.watcher, toast.type, toast.message] = [
        Math.random(),
        "success",
        "Ask Is Successfully Placed",
      ];
      rootState.loading = false;
      router.go(-1);
    });
    socket.on("twice ask", () => {
      rootState.loading = false;
      [toast.watcher, toast.type, toast.message] = [
        Math.random(),
        "error",
        "Cannot Place The Same Ask Twice",
      ];
    });
  },
  // UPDATE BID
  updateBid: (state, { bidAmount, rootState }) => {
    rootState.loading = true;
    const toast = rootState.toast;
    socket.emit("updatebid", {
      userids: localStorage.getItem("userId"),
      usernames: localStorage.getItem("userName"),
      productids: state.productId,
      productnames: state.productName,
      bid: bidAmount,
    });
    socket.on("bidupdated", () => {
      router.go(-1);
      rootState.loading = false;
      [toast.watcher, toast.type, toast.message] = [
        Math.random(),
        "success",
        "Bid is successfully Updated",
      ];
    });
  },
  // UPDATE ASK
  updateAsk: (state, { askAmount, rootState }) => {
    rootState.loading = true;
    const toast = rootState.toast;
    socket.emit("updateask", {
      userids: localStorage.getItem("userId"),
      usernames: localStorage.getItem("userName"),
      productids: state.productId,
      productnames: state.productName,
      ask: askAmount,
    });
    socket.on("askupdated", () => {
      router.go(-1);
      rootState.loading = false;
      [toast.watcher, toast.type, toast.message] = [
        Math.random(),
        "success",
        "Ask is successfully Updated",
      ];
    });
  },
  compareBestBid: (state, rootState) => {
    const toast = rootState.toast;
    rootState.loading2 = true;
    let productids = state.productId;
    socket.emit("comparebestbid", productids);
    socket.on("comparebestbid2", (data) => {
      const check = "";
      rootState.loading2 = false;
      localStorage.setItem("data", data);
      console.log("compare best bid: ", data);
      if (typeof data == typeof check) {
        [toast.watcher, toast.type, toast.message] = [
          Math.random(),
          "error",
          "Best Price Does Not Exist!",
        ];
      } else {
        [toast.watcher, toast.type, toast.message] = [
          Math.random(),
          "success",
          "Order Executed With Best Price",
        ];
        socket.emit("deletebestbid", {
          id: data[0].Id,
          productid: data[0].Product_Id,
        });
        socket.emit("updateordersbestbid", {
          buyerid: data[0].User_Id,
          buyername: data[0].User_Name,
          sellerid: localStorage.getItem("userId"),
          sellername: localStorage.getItem("userName"),
          price: data[0].Bid,
          productid: data[0].Product_Id,
          productname: data[0].Product_Name,
        });
        router.go(-1);
      }
    });
  },
  compareBestAsk: (state, rootState) => {
    const toast = rootState.toast;
    rootState.loading2 = true;
    socket.emit("comparebestask", {
      productids: state.productId,
      userids: localStorage.getItem("userId"),
    });
    socket.on("comparebestask2", (data) => {
      const check = "";
      rootState.loading2 = false;
      console.log("comparing best ask: ", data);
      if (typeof data == typeof check) {
        [toast.watcher, toast.type, toast.message] = [
          Math.random(),
          "error",
          "Best Price For Ask Does Not Exist",
        ];
      } else {
        [toast.watcher, toast.type, toast.message] = [
          Math.random(),
          "success",
          "Order Executed Best Ask Price",
        ];
        socket.emit("deletebestask", {
          id: data[0].Id,
          productid: data[0].Product_Id,
        });
        socket.emit("updateordersbestask", {
          buyerid: localStorage.getItem("userId"),
          buyername: localStorage.getItem("userName"),
          sellerid: data[0].User_Id,
          sellername: data[0].User_Name,
          price: data[0].Ask,
          productid: data[0].Product_Id,
          productname: data[0].Product_Name,
        });
        router.go(-1);
      }
    });
  },
  matchOrder: (state, rootState) => {
    socket.on("compare", ({ ask1, bid1 }) => {});
  },
};
