export default {
  mostPopular: ({ mostPopular }) =>
    mostPopular.map(({ Id, Image_Name, P_Name, Price }) => {
      return { id: Id, img: Image_Name, title: P_Name, price: Price };
    }),
  banners: ({ banners }) =>
    banners.map(({ Image_Name }) => {
      return { bannerImg: Image_Name };
    }),
  popularProducts: ({ popularProducts }) =>
    popularProducts.map(({ Id, Image_Name, P_Name, Retail }) => {
      return {
        id: Id,
        img: Image_Name,
        name: P_Name,
        price: Retail,
      };
    }),
};
