import Vue from "vue";
import Vuex from "vuex";
import axios from "axios";

// MODULES
import register from "./register/index";
import sellerDashboard from "./sellerDashboard/index";
import product from "./Product/index";
import landing from "./landingPage/index";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    loading: false,
    loading2: false,
    prevRoute: "skdf",
    toast: {
      type: "",
      message: "",
      watcher: 0,
    },
  },
  mutations: {},
  actions: {},
  modules: {
    register,
    sellerDashboard,
    product,
    landing,
  },
});
