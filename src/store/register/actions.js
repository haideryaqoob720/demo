export default {
  login: ({ commit, rootState }) => {
    commit("login", rootState);
  },
  signup: ({ commit, rootState }) => {
    commit("signup", rootState);
  },
};
