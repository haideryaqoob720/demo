import router from "../../router/index";

const validateLogin = ({ loginEmail, loginPassword }) =>
  loginEmail && loginPassword;

const validateSignup = ({
  signupFirstName,
  signupLastName,
  signupEmail,
  signupPassword,
}) => signupFirstName && signupLastName && signupEmail && signupPassword;

const clearLogin = (state) => {
  state.loginEmail = "";
  state.loginPassword = "";
};

const clearSignup = (state) => {
  state.signupFirstName = "";
  state.signupLastName = "";
  state.signupEmail = "";
  state.signupPassword = "";
};

export default {
  login: (state, rootState) => {
    const toast = rootState.toast;
    rootState.loading = true;
    const loginCredentials = {
      email: state.loginEmail,
      psswrd: state.loginPassword,
    };
    if (!validateLogin(state)) {
      rootState.loading = false;
      [toast.watcher, toast.type, toast.message] = [
        Math.random(),
        "error",
        "All the fields are required",
      ];
    } else {
      axios
        .post("/signin/seller", loginCredentials)
        .then(({ data }) => {
          rootState.loading = false;
          localStorage.setItem("userId", data.row[0].Id);
          localStorage.setItem("userName", data.row[0].First_Name);
          localStorage.setItem("token", data.token);
          [toast.watcher, toast.type, toast.message] = [
            Math.random(),
            "success",
            "You've successfully logged in",
          ];
          clearLogin(state);
          if (rootState.prevRoute.name == ("Buy" || "Sell")) {
            router.go(-1);
          } else {
            router.push({ name: "Buying" });
          }
        })
        .catch(() => {
          rootState.loading = false;
          [toast.watcher, toast.type, toast.message] = [
            Math.random(),
            "error",
            "Invalid Credentials",
          ];
        });
    }
  },
  signup: (state, rootState) => {
    rootState.loading = true;
    const toast = rootState.toast;
    const signupCredentials = {
      f_name: state.signupFirstName,
      l_name: state.signupLastName,
      email: state.signupEmail,
      psswrd: state.signupPassword,
    };
    if (!validateSignup(state)) {
      rootState.loading = false;
      [toast.watcher, toast.type, toast.message] = [
        Math.random(),
        "error",
        "All the fields are required",
      ];
    } else {
      axios
        .post("/signup/buyer", signupCredentials)
        .then(() => {
          rootState.loading = false;
          clearSignup(state);
          [toast.watcher, toast.type, toast.message] = [
            Math.random(),
            "success",
            "You've successfully signed up",
          ];
        })
        .catch(() => {
          rootState.loading = false;
          [toast.watcher, toast.type, toast.message] = [
            Math.random(),
            "error",
            "Invalid Credentials",
          ];
        });
    }
  },
};
