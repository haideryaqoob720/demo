import router from "./../../router/index";

const udpateProfileValidity = ({
  updateFirstName,
  updateLastName,
  updateEmail,
  updatePassword,
}) => updateFirstName && updateLastName && updateEmail && updatePassword;

const clearUpdateProfile = (state) => {
  state.updateFirstName = "";
  state.updateLastName = "";
  state.updateEmail = "";
  state.updatePassword = "";
};

export default {
  getProfile: (state, rootState) => {
    rootState.loading = true;
    axios
      .get("/user/9")
      .then(({ data }) => {
        rootState.loading = false;
        state.id = data[0].Id;
        state.firstName = data[0].First_Name;
        state.lastName = data[0].Last_Name;
        state.email = data[0].Email;
        state.password = data[0].User_Password;
      })
      .catch((error) => {
        console.log("profile error: ", error);
      });
  },
  updateProfile: (state, rootState) => {
    rootState.loading = true;
    let toast = rootState.toast;
    const updateProfileCredentials = {
      f_name: state.updateFirstName,
      l_name: state.updateLastName,
      email: state.updateEmail,
      password: state.updatePassword,
    };
    if (!udpateProfileValidity(state)) {
      rootState.loading = false;
      [toast.watcher, toast.type, toast.message] = [
        Math.random(),
        "error",
        "All the fields are required",
      ];
    } else {
      axios
        .put(
          `/signin/updateUser/${localStorage.getItem("userId")}`,
          updateProfileCredentials
        )
        .then((data) => {
          rootState.loading = false;
          router.go(-1);
          [toast.watcher, toast.type, toast.message] = [
            Math.random(),
            "success",
            "Profile has been updated",
          ];
          clearUpdateProfile(state);
        })
        .catch(() => {
          rootState.loading = false;
          [toast.watcher, toast.type, toast.message] = [
            Math.random(),
            "error",
            "Invalid Credentials",
          ];
        });
    }
  },
  getBuyings: (state, rootState) => {
    rootState.loading = true;
    console.log("GETTING BUYINGS");
    axios
      .get(`/userBids/${1}`)
      .then(({ data }) => {
        rootState.loading = false;
        state.buyings = data.result;
      })
      .catch(() => {
        console.log("ERROR GETTING BUYINGS");
      });
  },
  getSellings: (state, rootState) => {
    rootState.loading = true;
    console.log("GETTING SELLINGS");
    axios
      .get(`/userAsks/${1}`)
      .then(({ data }) => {
        rootState.loading = false;
        state.sellings = data.result;
      })
      .catch(() => {
        console.log("ERROR GETTING SELLINGS");
      });
  },
};
