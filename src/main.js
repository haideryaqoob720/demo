import Vue from "vue";
import VueMaterial from "vue-material";
import "vue-material/dist/vue-material.min.css";

import "./plugins/axios";
import App from "./App.vue";
import router from "./router";
import store from "./store/index";
import i18n from "./i18n";
import Toasted from "vue-toasted";

const ToastedOptions = {
  position: "bottom-center",
  duration: 1500,
  theme: "toasted-primary",
};

Vue.use(Toasted, ToastedOptions);

Vue.use(VueMaterial);
Vue.config.productionTip = false;

new Vue({
  router,
  store,
  i18n,
  render: (h) => h(App),
}).$mount("#app");
